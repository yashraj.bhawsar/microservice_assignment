package com.bench.nagarro.addProductService.SpringSecurity;

import org.springframework.security.core.GrantedAuthority;

public class Autority implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4189260927138520706L;

	public String role;

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return this.role;
	}

	public Autority(String role) {
		super();
		this.role = role;
	}

}

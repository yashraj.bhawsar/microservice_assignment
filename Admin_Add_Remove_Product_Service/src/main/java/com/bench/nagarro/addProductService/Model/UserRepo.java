package com.bench.nagarro.addProductService.Model;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bench.nagarro.addProductService.Bean.User;

public interface UserRepo extends JpaRepository<User, Integer> {
	public User findByUserName(String userName);
}

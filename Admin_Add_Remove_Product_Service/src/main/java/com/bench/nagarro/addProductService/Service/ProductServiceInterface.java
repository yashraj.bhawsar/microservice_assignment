package com.bench.nagarro.addProductService.Service;

import java.util.List;

import com.bench.nagarro.addProductService.Bean.Product;
import com.bench.nagarro.addProductService.Dto.ProductDto;
import com.bench.nagarro.addProductService.ExceptionHandle.ProductNotFoundException;

public interface ProductServiceInterface {

	public List<Product> getAllProduct();

	public Product removeProductById(int id) throws ProductNotFoundException;

	Product getProductById(int id) throws ProductNotFoundException;

	Product saveProduct(ProductDto productDto);

}

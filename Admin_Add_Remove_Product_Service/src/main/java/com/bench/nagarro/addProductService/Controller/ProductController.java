package com.bench.nagarro.addProductService.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bench.nagarro.addProductService.Bean.Product;
import com.bench.nagarro.addProductService.Dto.ProductDto;
import com.bench.nagarro.addProductService.ExceptionHandle.ProductNotFoundException;
import com.bench.nagarro.addProductService.Service.ProductService;

@RestController
@RequestMapping("/Admin")
public class ProductController {

	@Autowired
	ProductService productService;
	@Autowired
	ProductDto productDto;

	@PostMapping("/")
	public ResponseEntity<Product> saveProduct(@RequestBody @Valid ProductDto productDto) {

		return new ResponseEntity<Product>(productService.saveProduct(productDto), HttpStatus.CREATED);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Product> removeProduct(@PathVariable int id) throws ProductNotFoundException {
		return new ResponseEntity<Product>(productService.removeProductById(id), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable int id) throws ProductNotFoundException {
		
		return new ResponseEntity<Product>(productService.getProductById(id), HttpStatus.OK);
	}

	@GetMapping("/allProducts")
	public ResponseEntity<List<Product>> getAllProducts() {
		return ResponseEntity.ok(productService.getAllProduct());
	}

}

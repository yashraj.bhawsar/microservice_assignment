package com.bench.nagarro.addProductService.Dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Component
public class ProductDto {
	private int id;
	@NotNull(message = "Prduct Name Should Not Be Null")
	@Length(min = 2, max = 20, message = "Product Name Must Have  2 to 20 characters")
	private String ProductName;

	@Override
	public String toString() {
		return "ProductDto [id=" + id + ", ProductName=" + ProductName + "]";
	}

	public ProductDto(int id, String productName) {
		super();
		this.id = id;
		ProductName = productName;
	}

	public ProductDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

}

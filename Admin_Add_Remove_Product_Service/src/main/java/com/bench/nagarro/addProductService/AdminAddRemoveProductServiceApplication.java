package com.bench.nagarro.addProductService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminAddRemoveProductServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminAddRemoveProductServiceApplication.class, args);
	}

}

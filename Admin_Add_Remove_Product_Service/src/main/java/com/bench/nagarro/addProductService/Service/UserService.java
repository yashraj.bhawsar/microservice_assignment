package com.bench.nagarro.addProductService.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bench.nagarro.addProductService.Bean.User;
import com.bench.nagarro.addProductService.Model.UserRepo;

@Service
public class UserService {
	@Autowired
	UserRepo userRepo;

	public void saveUser(User u) {
		userRepo.save(u);
	}

}

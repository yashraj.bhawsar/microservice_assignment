package com.bench.nagarro.addProductService.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bench.nagarro.addProductService.Bean.Product;
import com.bench.nagarro.addProductService.Dto.ProductDto;
import com.bench.nagarro.addProductService.ExceptionHandle.ProductNotFoundException;
import com.bench.nagarro.addProductService.Model.ProductInterface;

@Service
public class ProductService implements ProductServiceInterface {
	@Autowired
	ProductInterface productInterface;

	@Override
	public Product saveProduct(ProductDto productDto) {
		Product product = new Product();
		product.setProductName(productDto.getProductName());
		Product p = productInterface.save(product);
		return p;
	}

	@Override
	public List<Product> getAllProduct() {
		return productInterface.findAll();

	}

	@Override
	public Product removeProductById(int id) throws ProductNotFoundException {
		Optional<Product> p = productInterface.findById(id);
		if (p.isEmpty())
			throw new ProductNotFoundException("Product is not deleted beacuse No Product have id " + id);
		productInterface.deleteById(id);
		return p.get();

	}

	@Override
	public Product getProductById(int id) throws ProductNotFoundException {

		Optional<Product> p = productInterface.findById(id);
		if (p.isEmpty())
			throw new ProductNotFoundException("Product is not get beacuse No Product have id " + id);
		return p.get();
	}

}

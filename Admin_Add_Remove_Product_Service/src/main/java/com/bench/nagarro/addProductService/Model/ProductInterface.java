package com.bench.nagarro.addProductService.Model;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bench.nagarro.addProductService.Bean.Product;

public interface ProductInterface extends JpaRepository<Product, Integer> {

}

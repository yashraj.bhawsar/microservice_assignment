package com.bench.nagarro.addProductService.SpringSecurity;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bench.nagarro.addProductService.Service.SecurityUserDetialsService;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	@Autowired
	SecurityUserDetialsService securityUserDetialsService;

	@Autowired
	JwtUtils jwtutils;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		final String requestTokenHeader = request.getHeader("Authorization");

		System.out.println(requestTokenHeader + "haahah");
		String userName = null;
		String jwtToken = null;
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				userName = this.jwtutils.extractUsername(jwtToken);
			} catch (ExpiredJwtException e) {
				e.printStackTrace();
				System.out.println("JWT TOKEN HAS Expired");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("error invalid Token");
			}
		} else {
			System.out.println(userName);
			System.out.println(jwtToken);
			System.out.println("invalid Token , Not Start WITH Bearer String");
		}
		if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			final UserDetails userDetails = this.securityUserDetialsService.loadUserByUsername(userName);
			if (this.jwtutils.validateToken(jwtToken, userDetails)) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthentication);
			}
		} else {
			System.out.println("Token Is not Valid");
		}
		filterChain.doFilter(request, response);

	}

}

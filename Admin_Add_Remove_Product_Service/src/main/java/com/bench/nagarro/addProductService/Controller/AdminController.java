package com.bench.nagarro.addProductService.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bench.nagarro.addProductService.Bean.User;
import com.bench.nagarro.addProductService.Dto.JwtRequest;
import com.bench.nagarro.addProductService.Dto.JwtResponse;
import com.bench.nagarro.addProductService.Service.SecurityUserDetialsService;
import com.bench.nagarro.addProductService.Service.UserService;
import com.bench.nagarro.addProductService.SpringSecurity.JwtUtils;

@RestController
@RequestMapping("/Admin")
public class AdminController {
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	UserService userService;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	SecurityUserDetialsService securityUserDetialsService;

	@PostMapping("/registerAdmin")
	public void saverRegisterUser(@RequestBody User user) {
		System.out.println("heelo");
		User u = new User();

		u.setRole("ADMIN_ROLE");
		u.setUserName(user.getUserName());
		u.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userService.saveUser(u);
	}

	@PostMapping("/generate-token")
	public ResponseEntity<?> generateToken(@RequestBody JwtRequest jwtRequest) throws Exception {
		System.out.println(jwtRequest + "ahahha");
		try {
			authenticate(jwtRequest.getUserName(), jwtRequest.getPassword());

		} catch (UsernameNotFoundException e) {
			e.printStackTrace();
			throw new Exception("User not Found");
		}
		UserDetails userDetials = this.securityUserDetialsService.loadUserByUsername(jwtRequest.getUserName());

		String token = this.jwtUtils.generateToken(userDetials);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	private void authenticate(String userName, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));

		} catch (DisabledException e) {
			throw new Exception("User Disabled " + e.getMessage());

		} catch (BadCredentialsException e) {
			throw new Exception("Invalid Credentials " + e.getMessage());
		}
	}
}

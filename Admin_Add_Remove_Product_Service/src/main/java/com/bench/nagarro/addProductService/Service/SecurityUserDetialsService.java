package com.bench.nagarro.addProductService.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bench.nagarro.addProductService.Bean.User;
import com.bench.nagarro.addProductService.Model.UserRepo;

@Service
public class SecurityUserDetialsService implements UserDetailsService {

	@Autowired
	UserRepo userRepo;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		
		User user = userRepo.findByUserName(userName);
		if (user == null) {
			System.out.println("USER nOT fOUND");
			throw new UsernameNotFoundException("user is not found ");
		}
		return user;

	}

}
